
<br>

<body style="font-family: sans-serif" onload="window.print()">
<center>
    <table width="94%" border="0">
        <tr>
            <td rowspan="4" width="10%">
                <center><div><img src="{{ asset('assets/img/wk.png') }}" width="100px"></div></center>
            </td>
            <td>
                <b>PANITIA PENERIMAAN PESERTA DIDIK BARU</b><br>
                <b>SMK WIKRAMA BOGOR T.P. 2020-2021</b><br>
                Jl. Raya Wangun Kel. Sindangsari Kec. Bogor Timur Kota Bogor <br>
                Email : prohumasi@smkwikrama.sch.id
            </td>
        </tr>

    </table>
</center>
<br>
    <center><b>TANDA BUKTI PENDAFTARAN</b></center>
    <br>
    <table width="55%" border="0" style="margin-left:3%;margin-right:2%;float:left">
        <tr>
            <td colspan="3" style="background-color: lightgray"><center><b>BIODATA CALON PESERTA DIDIK</b></center></td>
        </tr>
        <tr>
            <td width="30%"><b>TANGGAL DAFTAR</b></td>
            <td width="3%">:</td>
            <td>{{ Carbon\Carbon::parse($siswa->created_at)->isoFormat('D MMMM Y') }}</td>
        </tr>
        <tr>
            <td width="30%"><b>NOMOR SELEKSI</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->id }}</td>
        </tr>
        <tr>
            <td width="30%"><b>NAMA LENGKAP</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->nama }}</td>
        </tr>
        <tr>
            <td width="30%"><b>JENIS KELAMIN</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->jenkel }}</td>
        </tr>
        <tr>
            <td width="30%"><b>NIS</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->nis }}</td>
        </tr>
        <tr>
            <td width="30%"><b>TEMPAT LAHIR</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->temp_lahir }}</td>
        </tr>
        <tr>
            <td width="30%"><b>TANGGAL LAHIR</b></td>
            <td width="3%">:</td>
            <td>{{ Carbon\Carbon::parse($siswa->tgl_lahir)->isoFormat('D MMMM Y') }}</td>
        </tr>
        <tr>
            <td width="30%"><b>ALAMAT</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->alamat }}</td>
        </tr>
        <tr>
            <td width="30%"><b>ASAL SEKOLAH</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->asal_sekolah }}</td>
        </tr>
        <tr>
            <td width="30%"><b>KELAS</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->kelas }}</td>
        </tr>
        <tr>
            <td width="30%"><b>JURUSAN</b></td>
            <td width="3%">:</td>
            <td>{{ $siswa->jurusan }}</td>
        </tr>
    </table>
    <table width="37%" border="0">
        <tr>
            <td colspan="3" style="background-color: lightgray"><center><b>PERSYARATAN</b></center></td>
        </tr>
    </table>
    <br>
    <table width="37%" border="0">
        <tr>
            <td><b>B. Foto/Scan Dokumen yang Harus Dipersiapkan</b></td>
        </tr>

        <tr>
            <td>
                1. Raport Semester 1 s.d 5 yang telah dilegasir dalam bentuk pdf(satu file)<br>
                2. Akta Kelahiran dalam bentuk pdf/jpeg(satu file)<br>
                3. KTP Ayah dan ibu dipisah dalam bentuk pdf/jpeg<br>
                4. Kartu Keluarga dalam bentuk pdf/jpeg<br>
                5. Kartu NISN dalam bentuk pdf/jpeg
            </td>
        </tr>

    </table>
    <br>
    <table width="37%" border="0" style="margin-left:60%;">
        <tr>
            <td colspan="3" style="background-color: white"><b>C. Biaya Seleksi</b></td>
        </tr>
        <tr>
            <td>Pembayaran uang seleksi sebesasr Rp. 175.588 melalui transfer bank:</td>
        </tr>
        <tr>
            <td>
                Bank BNI: 1147535806 A.N SMK Wikrama Sekolah.
            </td>
        </tr>
        <tr>
            <td>Pastikan nominal transfer dengan kode unik 3 digit terakhir sesuai dengan nomor seleksi</td>
        </tr>

    </table>
</body>
