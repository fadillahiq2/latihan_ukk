<?php

use App\Http\Controllers\DaftarController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/daftar', [DaftarController::class, 'daftar'])->name('daftar');
Route::post('/daftarPost', [DaftarController::class, 'daftar_post'])->name('daftarPost');
Route::get('/daftar/hasil/{id}', [DaftarController::class, 'daftar_hasil'])->name('daftarHasil');
Route::get('/daftar/hasil/edit/{id}', [DaftarController::class, 'daftar_edit'])->name('daftarEdit');
Route::put('/daftarUpdate/{id}', [DaftarController::class, 'daftar_update'])->name('daftarUpdate');
Route::get('/daftar/hasil/print/{id}', [DaftarController::class, 'daftar_print'])->name('daftarPrint');
Route::delete('/daftarDelete/{id}', [DaftarController::class, 'daftar_delete'])->name('daftarDelete');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
