<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DaftarController extends Controller
{
    public function daftar() {
        return view('daftar');
    }

    public function daftar_edit($id) {
        $siswa = Siswa::findOrFail($id);
        return view('daftar-edit', compact('siswa'));
    }

    public function daftar_hasil($id) {
        $siswa = Siswa::findOrFail($id);
        return view('daftar-hasil', compact('siswa'));
    }

    public function daftar_print($id) {
        $siswa = Siswa::findOrFail($id);
        return view('daftar-print', compact('siswa'));
    }

    public function daftar_post(Request $request) {
        $request->validate([
            'nis' => 'required|numeric',
            'nama' => 'required|max:50',
            'jenkel' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required'
        ]);

        $siswa = Siswa::create($request->all());

        $user = new User;
        $user->name = $request->nama;
        $user->email = $request->nis;
        $user->password = bcrypt('ppdb2021');
        $user->remember_token = Str::random(60);
        $user->save();

        return redirect()->route('daftarHasil',$siswa->id);
    }

    public function daftar_update(Request $request, $id) {
        $request->validate([
            'nis' => 'required|numeric',
            'nama' => 'required|max:50',
            'jenkel' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required'
        ]);
        $siswa = Siswa::findOrFail($id);
        $siswa->update($request->all());

        return redirect()->route('daftarHasil',$siswa->id);
    }

    public function daftar_delete($id) {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();

        return redirect()->route('daftar');
    }
}
